
import axios from 'axios';
import Global from '../Global';


const Request = {

    post: (subUrl, data) => {
        let method = 'POST';
        return RequestHandler.handelRequest(subUrl, data, method);
    },



    get: (subUrl, parameters) => {
        let method = 'GET';
        return RequestHandler.handelRequest(subUrl, parameters, method);
    },
}

// Here is the middle ware
// subUrl:string,data,method
const RequestHandler = {
    handelRequest: (subUrl, data, method, config) => {
    let url = '' ;
      if (Global.Environment == "DEV") {
            url = Global.Dev_URL + subUrl;
        } else if (Global.Environment == "PRODUCTION") {
            url = Global.Production_URL + subUrl;
        } else {
            console.error("Environment not detected");
            return null;
        }

        return new Promise((resolve, reject) => {
            axios({
              method, url, data, headers: {
                    'Content-Type':  Global.contentType
                }
            })
                .then(function (response) {

                    resolve(response)
                })
                .catch(function (error) {
                    reject(error)
                });
        });

    },
    handleResponse: (response) => { return response },

}



export default Request;
