import React from "react";
import ReactDOM from "react-dom";
import Login from './Login/Login'

const App = () => {
  return (
    <div>
      <Login />
    </div>
  );
};
export default App;
ReactDOM.render(<App />, document.getElementById("app"));
